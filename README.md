# CakePHP Application Skeleton

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.

To install all requirements, if Composer is installed globally, run
```
composer install
```

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Update

You can run this command to update all your requirements
```
composer update
```

## Configuration

Read and edit the environment specific `config/app_local.php` and setup the 
`'Datasources'` and any other configuration relevant for your application.
Other environment agnostic settings can be changed in `config/app.php`.
In `config/app_local.php` you will have to setup the database access. The 
database have to contain at least one user for you to be able to access
the website. You can see all tables in the `src/Model/Table` file.

## Layout

The app skeleton uses [Milligram](https://milligram.io/) (v1.3) minimalist CSS
framework.

## Access

You can access all pages through the 
```
/[tableName]/tojson
```
To get data in json format.